package com.formation.hadoop.MapReduceBase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class WordCount extends Configured implements Tool{

	public int run(String[] args) throws Exception{
		
		if (args.length != 2) {
			System.out.println("Usage: [input] [output]");
			System.exit(-1);
		}
		
		Configuration conf = getConf();
		//conf.set("fs.default.name", "file:///");
		conf.set("mapred.job.tracker", "local");
		//conf.set("fs.file.impl", "com.formation.hadoop.MapReduceBase.fs.WindowsLocalFileSystem");
		conf.set("io.serializations", "org.apache.hadoop.io.serializer."
		+ "JavaSerialization," 
		+ "org.apache.hadoop.io.serializer.WritableSerialization");
		
		// on créer un nouvel object hadoop Job
		Job job = Job.getInstance(conf);
		job.setJobName("wordcount");
		
		// indique à hadoop par le biais de l'object job quelles sont les classes
		// driver, map et reduce de notre programme Hadoop
		job.setJarByClass(WordCount.class);
		job.setMapperClass(WordMapper.class);
		job.setReducerClass(WordReducer.class);
		
		// on indique à hadoop quels sont les types de données que l'on souhaite utiliser
		// pour les couples (clé; valeur) de nos opérations map et reduce
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		// on indique où se situent nos données d'entrée et de sortie dans HDFS
		// on utilise les classes hadoop FileInputFormat et FileOutputFormat
		
		Path inputFilePath = new Path(args[0]);
		Path outputFilePath = new Path(args[1]);
		FileInputFormat.addInputPath(job, inputFilePath);
		FileOutputFormat.setOutputPath(job, outputFilePath);
		
		// on indique à Hadoop quels sont les formats de données d'entrée et de sortie
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setInputFormatClass(TextInputFormat.class);
		
		// lance le job
		return job.waitForCompletion(true) ? 0 : 1;
	}
	
	public static void main(String[] args) throws Exception {	
		
		WordCount wordcountDriver = new WordCount();
		int res = ToolRunner.run(wordcountDriver, args);
		System.exit(res);
	}

}
