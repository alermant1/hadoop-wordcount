package com.formation.hadoop.MapReduceBase;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WordMapper extends Mapper<LongWritable, Text, Text, IntWritable>{
	// LongWritable: Input key type
	// Text: Input value Type
	// Text: output key type
	// IntWritable: output value type
	
	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {

		String s = value.toString();
		String[] values = s.split(" ");
		
		for (String word: values) {
			if (word.length() > 0) {
				context.write(new Text(word), new IntWritable(1));
			}
		}
	}
	
}
