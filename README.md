## Build le projet
```
$ mvn clean install
```

## Créer un fichier sample.txt

```
Hello Hadoop
Goodbye Hadoop
Good morning my friend
Nice to meet you my friend
```

## Déplacer le fichier dans hdfs

```
$ hadoop fs -put sample.txt /sample.txt
```

## Executer le jar

```
$ cd target
$ hadoop jar MapReduceBase-1.jar com.formation.hadoop.MapReduceBase.WordCount /sample.txt /output
```

## Afficher le résultat

```
$ hadoop fs -cat /output/*
``` 
